var products = [{
   name:"Black T-shirt",
   oldPrice: 123.20,
   newPrice: 50.99,
   image: "images/product/BlackI.jpg",
   link: "ClothesDetail.html"

}, { 
   name:"White T-shirt",
   oldPrice: 100,
   newPrice: 64.99,
   image: "images/product/BlackC.jpg",
   link: "ClothesDetail.html"
}, {
   name:"Pink T-shirt",
   oldPrice: 123.20,
   newPrice: 91.99,
   image: "images/product/BlackC.jpg",
   link: "ClothesDetail.html"
}, {
   name:"Grey T-shirt",
   oldPrice: 123.20,
   newPrice: 43.99,
   image: "images/product/BlackD.jpg",
   link: "ClothesDetail.html"
}, {
   name:"Yellow T-shirt",
   oldPrice: 123.20,
   newPrice: 52.99,	
   image: "images/product/BlackC.jpg",
   link: "ClothesDetail.html"
}, {
   name:"Green T-shirt",
   oldPrice: 123.20,
   newPrice: 60.99,
   image: "images/product/BlackI.jpg",
   link: "ClothesDetail.html"
}, {
   name:"Blue T-shirt",
   oldPrice: 123.20,
   newPrice: 64.99,
   image: "images/product/BlackH.jpg",
   link: "ClothesDetail.html"
}, {
   name:"Orange T-shirt",
   oldPrice: 123.20,
   newPrice: 99.99,
   image: "images/product/BlackD.jpg",
   link: "ClothesDetail.html"
}, {
   name:"Red T-shirt",
   oldPrice: 123.20,
   newPrice: 33.99,
   image: "images/product/BlackH.jpg",
   link: "ClothesDetail.html"
}];

$(function() {
	$("#sort").change(function() {
		if ($("#sort").val() == 1) {
			products.sort(function(a, b) {
              			return a.name.localeCompare(b.name);
          		});
		} else if ($("#sort").val() == 2) {
      			products.sort(function(a, b) {
         			return b.name.localeCompare(a.name);
     			});
   		} else if ($("#sort").val() == 3) {
      			products.sort(function(a, b) {
         			return a.newPrice - b.newPrice;
     			});     
   		} else if ($("#sort").val() == 4) {
      			products.sort(function(a, b) {
					 return b.newPrice - a.newPrice;					
				 });
				 
   		}

		console.log(products);

		var productsHtml = '';

		for (var product of products) {
			var productHtml = '<li>' +
				'<div class="item col-md-4 col-sm-6 col-xs-6">' +
				'<div class="product-block">' +
				'<div class="image"> <a href= " ' + product.link + ' "><img class="img-responsive" title="T-shirt" alt="T-shirt" src="' + product.image + '"></a> </div>' +
				'<div class="product-details">' +
				'<div class="product-name">' +
				'<h4><a href= " ' + product.link + ' ">' + product.name + '</a></h4>' +
				'</div>' +
				'<div class="price"> <span class="price-old">$' + product.oldPrice + '</span> <span class="price-new">' + product.newPrice + '</span> </div>' +
				'<div class="product-hov">' +
				'<ul>' +
				'<li class="wish"><a href="#"></a></li>' +
				'<li class="addtocart"><a href="#">Add to Cart</a> </li>' +
				'<li class="compare"><a href="#"></a></li>' +
				'</ul>' +
				'<div class="review"> <span class="rate"> <i class="fa fa-star rated"></i> <i class="fa fa-star rated"></i> <i class="fa fa-star rated"></i> <i class="fa fa-star rated"></i> <i class="fa fa-star"></i> </span> </div>' +
				'</div>' +
				'</div>' +
				'</div>' +
				'</div>' +
				'</li>';
			
			productsHtml += productHtml;
		}

		$('#product-list').html(productsHtml);
	});
	$('#sort').change();
});
