var products = [{
	name:"Television",
	oldPrice: 1500.00,
	newPrice: 1399.99,
	image: "images/product/television.jpg",
	link: "ElectronicDetail.html"
 }, { 
	name:"Air Conditioners",
	oldPrice: 3000.00,
	newPrice: 2500.00,
	image: "images/product/aircon.jpg",
	link: "ElectronicDetail.html"
 }, {
	name:"Toasters",
	oldPrice: 15.00,
	newPrice: 12.99,
	image: "images/product/toaster.jpg",
	link: "ElectronicDetail.html"
 }, {
	name:"Ovens",
	oldPrice: 500.00,
	newPrice: 300.00,
	image: "images/product/oven.jpg",
	link: "ElectronicDetail.html"
 }, {
	name:"Gas Stoves",
	oldPrice: 450.00,
	newPrice: 250.00,	
	image: "images/product/gas.jpg",
	link: "ElectronicDetail.html"
 }, {
	name:"Vaccum",
	oldPrice: 1550.00,
	newPrice: 1199.99,
	image: "images/product/vaccum.jpg",
	link: "ElectronicDetail.html"
 }, {
	name:"Blender",
	oldPrice: 160.00,
	newPrice: 139.99,
	image: "images/product/blender.jpg",
	link: "ElectronicDetail.html"
 }, {
	name:"Amplifier",
	oldPrice: 123.20,
	newPrice: 99.99,
	image: "images/product/amplifier.jpg",
	link: "ElectronicDetail.html"
 }, {
	name:"Heater",
	oldPrice: 500.00,
	newPrice: 450.00,
	image: "images/product/heater.jpg",
	link: "ElectronicDetail.html"
 }];

$(function() {
	$("#sort").change(function() {
		if ($("#sort").val() == 1) {
			products.sort(function(a, b) {
              			return a.name.localeCompare(b.name);
          		});
		} else if ($("#sort").val() == 2) {
      			products.sort(function(a, b) {
         			return b.name.localeCompare(a.name);
     			});
   		} else if ($("#sort").val() == 3) {
      			products.sort(function(a, b) {
         			return a.newPrice - b.newPrice;
     			});     
   		} else if ($("#sort").val() == 4) {
      			products.sort(function(a, b) {
					 return b.newPrice - a.newPrice;					
				 });
				 
   		}

		console.log(products);

		var productsHtml = '';

		for (var product of products) {
			var productHtml = '<li>' +
				'<div class="item col-md-4 col-sm-6 col-xs-6">' +
				'<div class="product-block">' +
				'<div class="image"> <a href= " ' + product.link + ' "><img class="img-responsive" title="T-shirt" alt="T-shirt" src="' + product.image + '"></a> </div>' +
				'<div class="product-details">' +
				'<div class="product-name">' +
				'<h4><a href= " ' + product.link + ' ">' + product.name + '</a></h4>' +
				'</div>' +
				'<div class="price"> <span class="price-old">$' + product.oldPrice + '</span> <span class="price-new">' + product.newPrice + '</span> </div>' +
				'<div class="product-hov">' +
				'<ul>' +
				'<li class="wish"><a href="#"></a></li>' +
				'<li class="addtocart"><a href="#">Add to Cart</a> </li>' +
				'<li class="compare"><a href="#"></a></li>' +
				'</ul>' +
				'<div class="review"> <span class="rate"> <i class="fa fa-star rated"></i> <i class="fa fa-star rated"></i> <i class="fa fa-star rated"></i> <i class="fa fa-star rated"></i> <i class="fa fa-star"></i> </span> </div>' +
				'</div>' +
				'</div>' +
				'</div>' +
				'</div>' +
				'</li>';
			
			productsHtml += productHtml;
		}

		$('#product-list').html(productsHtml);
	});
	$('#sort').change();
});