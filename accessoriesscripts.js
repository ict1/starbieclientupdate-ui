var products = [{
	name:"Rings",
	oldPrice: 200.00,
	newPrice: 100.00,
	image: "images/product/rings.jpg",
	link: "AccessoriesDetail.html"
 }, { 
	name:"Bracelets",
	oldPrice: 100,
	newPrice: 50.99,
	image: "images/product/bracelets.jpg",
	link: "AccessoriesDetail.html"
 }, {
	name:"Necklaces",
	oldPrice: 230.00,
	newPrice: 200.00,
	image: "images/product/necklaces.jpg",
	link: "AccessoriesDetail.html"
 }, {
	name:"Glasses",
	oldPrice: 300.00,
	newPrice: 199.99,
	image: "images/product/glasses.jpg",
	link: "AccessoriesDetail.html"
 }, {
	name:"Hats",
	oldPrice: 50.00,
	newPrice: 25.00,	
	image: "images/product/hats.jpg",
	link: "AccessoriesDetail.html"
 }, {
	name:"Sunglasses",
	oldPrice: 200.00,
	newPrice: 149.99,
	image: "images/product/sunglasses.jpg",
	link: "AccessoriesDetail.html"
 }, {
	name:"Earrings",
	oldPrice: 30.00,
	newPrice: 25.00,
	image: "images/product/earrings.jpg",
	link: "AccessoriesDetail.html"
 }, {
	name:"Foot bracelets",
	oldPrice: 20.00,
	newPrice: 15.00,
	image: "images/product/footbracelets.jpg",
	link: "AccessoriesDetail.html"
 }, {
	name:"Belts",
	oldPrice: 50.00,
	newPrice: 29.99,
	image: "images/product/belts.jpg",
	link: "AccessoriesDetail.html"
 }];

$(function() {
	$("#sort").change(function() {
		if ($("#sort").val() == 1) {
			products.sort(function(a, b) {
              			return a.name.localeCompare(b.name);
          		});
		} else if ($("#sort").val() == 2) {
      			products.sort(function(a, b) {
         			return b.name.localeCompare(a.name);
     			});
   		} else if ($("#sort").val() == 3) {
      			products.sort(function(a, b) {
         			return a.newPrice - b.newPrice;
     			});     
   		} else if ($("#sort").val() == 4) {
      			products.sort(function(a, b) {
					 return b.newPrice - a.newPrice;					
				 });
				 
   		}

		console.log(products);

		var productsHtml = '';

		var productsHtml = '';

		for (var product of products) {
			var productHtml = '<li>' +
				'<div class="item col-md-4 col-sm-6 col-xs-6">' +
				'<div class="product-block">' +
				'<div class="image"> <a href= " ' + product.link + ' "><img class="img-responsive" title="T-shirt" alt="T-shirt" src="' + product.image + '"></a> </div>' +
				'<div class="product-details">' +
				'<div class="product-name">' +
				'<h4><a href= " ' + product.link + ' ">' + product.name + '</a></h4>' +
				'</div>' +
				'<div class="price"> <span class="price-old">$' + product.oldPrice + '</span> <span class="price-new">' + product.newPrice + '</span> </div>' +
				'<div class="product-hov">' +
				'<ul>' +
				'<li class="wish"><a href="#"></a></li>' +
				'<li class="addtocart"><a href="#">Add to Cart</a> </li>' +
				'<li class="compare"><a href="#"></a></li>' +
				'</ul>' +
				'<div class="review"> <span class="rate"> <i class="fa fa-star rated"></i> <i class="fa fa-star rated"></i> <i class="fa fa-star rated"></i> <i class="fa fa-star rated"></i> <i class="fa fa-star"></i> </span> </div>' +
				'</div>' +
				'</div>' +
				'</div>' +
				'</div>' +
				'</li>';
			
			productsHtml += productHtml;
		}

		$('#product-list').html(productsHtml);
	});
	$('#sort').change();
});